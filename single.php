<?php get_header(); ?>
<?php
  $author_avatar_url = get_avatar_url(
    get_the_author_meta('ID', false),
    array(
      'size' => 64
    ));
?>

<div class="container">
    <div class="row">
      <div class="col">
        <h1 class="h2 mb-2"><?php the_title(); ?></h1>
      </div>
    </div>

    <div class="row">
      <div class="col">
        <p class="text-muted">Last updated on <?php the_date();?> by <a href="" class="text-primary text-decoration-none"><?php the_author();?></a></p>
      </div>
    </div>
    <hr />
    <?php if (have_posts()) : while(have_posts()) : the_post(); ?>
      <div class="row">
        <div class="col">
          <?php the_content(); ?>
        </div>
      </div>
    <?php endwhile; endif; ?>
</div>


<?php get_footer(); ?>
