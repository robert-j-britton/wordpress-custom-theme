<?php get_header(); ?>


<div class="container">

  <h1 class="text-left h2 my-4">Recent Posts</h1>


  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <div class="card mb-3">
        <div class="card-body">
          <h2 class="h3 mb-2"><?php the_title(); ?></h2>
          <?php the_excerpt(); ?>
          <a href="<?php the_permalink(); ?>" class="btn btn-primary">Read more</a>
        </div>
      </div>
  <?php endwhile;
  endif; ?>
</div>


<?php get_footer(); ?>
