<?php get_header(); ?>


<div class="container">

  <h1 class="h2 text-center mb-2"><?php single_cat_title(); ?></h1>

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <div class="card mb-2">
        <div class="card-body">
          <h2 class="h3 mb-2">
            <a href="<?php the_permalink(); ?>" class="text-dark">
              <?php the_title(); ?>
            </a>
          </h2>
          <?php the_excerpt(); ?>
          <a href="<?php the_permalink(); ?>" class="btn btn-primary">Read more</a>
        </div>
      </div>
  <?php endwhile;
  endif; ?>
</div>


<?php get_footer(); ?>
