<?php

function load_stylesheets()
{

  wp_register_style(
    'style',
    get_template_directory_uri() . '/style.css',
    array(),
    false,
    'all'
  );

  wp_enqueue_style('style');
}


function load_scripts()
{
  wp_register_script(
    'bootstrap_bundle',
    get_template_directory_uri() . '/bootstrap/js/bootstrap.bundle.min.js',
    '',
    1,
    true
  );

  wp_enqueue_script('bootstrap_bundle');
}

/**
 * Register Custom Navigation Walker
 */
function register_navwalker()
{
  require_once get_template_directory() . '/classes/class-wp-bootstrap-navwalker.php';
}

add_action('wp_enqueue_scripts', 'load_stylesheets');
add_action('wp_enqueue_scripts', 'load_scripts');
add_action('after_setup_theme', 'register_navwalker');
add_theme_support('menus');

register_nav_menus(
  array(
    'top-menu' => __('Top Menu'),
    'footer-menu' => __('Footer Menu')
  )
);

// Add support for featured image
add_theme_support( 'post-thumbnails' );
