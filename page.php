<?php get_header(); ?>


<div class="container">



  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div class="row align-center">
    <div class="col">
      <h1 class="h2 mb-2"><?php the_title(); ?></h1>
      <?php the_content(); ?>

      <?php
        // If comments are open or we have at least one comment, load up the comment template.
        if ( comments_open() || get_comments_number() ) {
          comments_template();
        }
      ?>
    </div>
  </div>


  <?php endwhile;
  endif; ?>
</div>


<?php get_footer(); ?>
