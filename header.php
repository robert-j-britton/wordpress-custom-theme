<!DOCTYPE html>

<head>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-4">
      <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigationBar" aria-controls="bs-example-navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="/">
          <?php if ( has_custom_logo() ) : ?>
            <div class="d-inline-block align-top"><?php the_custom_logo(); ?></div>
          <?php endif; ?>
          <?php bloginfo( 'name' ) ?>
        </a>

        <div class="collapse navbar-collapse" id="navigationBar">
          <?php
            wp_nav_menu(
              array(
                'theme_location' => 'top-menu',
                'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
                'container'       => null,
                'container_class' => null,
                'container_id'    => '',
                'menu_class'      => 'navbar-nav ml-auto',
                'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                'walker'          => new WP_Bootstrap_Navwalker(),
              )
            );
          ?>
          <form class="form-inline my-2 my-lg-0" method="get" action="/">
            <input class="form-control mr-sm-2" name="s" type="search" placeholder="Search" aria-label="Search" value="<?php the_search_query(); ?>">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
          </form><!-- Search -->
        </div>

      </div>
    </nav>
  </header>
